# سبحانك ربي عدد خلقك و رضا نفسك و زنة عرشة و مداد كلماتك
# بسم اللّه الرحمن الرحيم
#%%
import torch
from torch import nn, optim, Tensor
from torch.nn.modules.dropout import Dropout
import torchvision
from torchvision import models, transforms, datasets
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image
import json

mean_vals = [0.485, 0.456, 0.406]
std_vals = [0.229, 0.224, 0.225]

train_dir = r"E:\important_files_py\Python Scripts\Artificial_Intelligence\img_datasets\train"
test_dir = r"E:\important_files_py\Python Scripts\Artificial_Intelligence\img_datasets\test"
valid_dir = r"E:\important_files_py\Python Scripts\Artificial_Intelligence\img_datasets\valid"

# Transforms

trn_img_trnsfrms = transforms.Compose([transforms.RandomRotation(30),
                                        transforms.RandomResizedCrop(224),
                                        transforms.RandomHorizontalFlip(),
                                        transforms.ToTensor(), 
                                        transforms.Normalize(mean= mean_vals, std=std_vals)])

tst_img_trnsfrms = transforms.Compose([transforms.Resize(224),
                                       transforms.CenterCrop(224),
                                       transforms.ToTensor(),
                                       transforms.Normalize(mean=mean_vals, std=std_vals)])

vld_img_trnsfrms = transforms.Compose([transforms.Resize(255), 
                                       transforms.CenterCrop(224),
                                       transforms.ToTensor(), 
                                       transforms.Normalize(mean=mean_vals, std=std_vals)])
# Datasets Definition
trn_img_dataset = datasets.ImageFolder(train_dir, transform=trn_img_trnsfrms)

tst_img_dataset = datasets.ImageFolder(test_dir, transform=tst_img_trnsfrms)

vld_img_dataset = datasets.ImageFolder(valid_dir, transform=vld_img_trnsfrms)
# json file
with open(r"E:\important_files_py\Python Scripts\Artificial_Intelligence\AI_Programming_NanoDegree\cat_to_name.json", 'r') as file:
    cat_to_name = json.load(file)



# Loading images as Tensors
trn_img_loader = torch.utils.data.DataLoader(trn_img_dataset, batch_size = 3, shuffle=True)
tst_img_loader = torch.utils.data.DataLoader(tst_img_dataset, batch_size = 3)
vld_img_loader = torch.utils.data.DataLoader(vld_img_dataset, batch_size = 3)

# looping through iter-- ▌▌ Implementation for this part is postponed.
#img, lbl = next(iter(trn_img_loader))


# model definition
img_prcs_mdl = models.vgg16(pretrained = True)

for param in img_prcs_mdl.parameters():
    param.requires_grad = False

img_prcs_classifier = nn.Sequential(nn.Linear(25088, 7104), nn.ReLU(),nn.Dropout(0.2),
                                    nn.Linear(7104, 2), nn.LogSoftmax(dim=1))
                                 

'''
    ▬ Working on image classification.
        ▌ NN Training phase
'''
img_prcs_mdl.classifier = img_prcs_classifier
mdl_lss_func = nn.NLLLoss()
mdl_optimiser = optim.SGD(img_prcs_mdl.classifier.parameters(), lr= 0.003)

epochs = 5
step = 0    
ttl_loss = 0
batch_count = 5
# • Training phase.

for i in range(1):

    for img, lbl in trn_img_loader:
        step += 1
        prcsd_img = img_prcs_mdl(img)
        lss_val = mdl_lss_func(prcsd_img, lbl)
        mdl_optimiser.zero_grad()
        lss_val.backward()
        mdl_optimiser.step()
        
        ttl_loss += lss_val

            # Validating for each 5 batches
        if step % batch_count == 0:
            tst_lss_val = 0
            accuracy = 0
            img_prcs_mdl.eval()

            with torch.no_grad():
                for img, lbls in vld_img_loader:
                    probe_vals = img_prcs_mdl(img)
                    lss_val = mdl_lss_func(probe_vals, lbls)
                    tst_lss_val += lss_val.item()
                    probes = torch.exp(probe_vals)
                    top_probe, top_clss = probes.topk(1, dim=1)
                    probe_corel = top_clss == lbls.view(*top_clss.shape)
                    accuracy += torch.mean(probe_corel.type(torch.FloatTensor)).item()
            print(f"Validation accuracy: {accuracy/len(vld_img_loader)}")
            print(f"Training loss: {ttl_loss/batch_count}")
        

            ttl_loss = 0
            img_prcs_mdl.train()
    
'''
        ▌ Network Validation phase:
'''
tot_test_loss = 0
test_correct = 0  # Number of correct predictions on the test set
        
        # Turn off gradients for validation, saves memory and computations
with torch.no_grad():
    img_prcs_mdl.eval()
    for images, labels in tst_img_loader:
            log_ps = img_prcs_mdl(images)
            loss = mdl_lss_func(log_ps, labels)
            tot_test_loss += loss.item()

            ps = torch.exp(log_ps)
            top_p, top_class = ps.topk(1, dim=1)
            equals = top_class == labels.view(*top_class.shape)
            test_correct += torch.mean(equals.type(torch.FloatTensor))
    #img_prcs_mdl.train()
    print("Test Accuracy: {:.3f}".format(test_correct / len(tst_img_loader)))

'''
        ▌ Network Inference
'''


# for plotting implementation.    
def get_key(dic, value):
    for key, val in dic.items():
        if val == value:
            return key

def map_dict(top_clss, categ_json_fl, img_dataset):
    top_clss.squeeze_(0)
    top_clss = top_clss.numpy()
    img_dataset_idx = img_dataset.class_to_idx
    mapped_dic = {}
    for i in top_clss:
        if i in img_dataset_idx.values():
            mapped_dic[get_key(img_dataset_idx,i)] = categ_json_fl[get_key(img_dataset_idx,i)]
    return mapped_dic

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
def deduct(image_path, model, topk=5):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''
    img = Image.open(image_path)
    data_transforms = transforms.Compose([transforms.Resize((224,224)), 
                                           transforms.ToTensor(), 
                                           transforms.Normalize(mean=mean_vals, std=std_vals)])
    
    # get a tensor structure for the specified format of image.
    img_tnsr = data_transforms(img)
    
    # transform the tensor to be processed on the gpu alternatively.
    img_tnsr = img_tnsr.to(device)
    
    #to remove a dimension of a tensor for example transform a 2D tensor into 1D tensor. __USE <tensor.unsqueeze()>
    img_tnsr.unsqueeze_(0)
    
    # receiving a gpu sturct. expecting cpu structure of image so the structure should be transferred into the cpu instead.
    img_tnsr = img_tnsr.cpu().clone()
    probe_vals = torch.exp(model(img_tnsr))

    top_probe, top_clss = probe_vals.topk(topk, dim=1)
    top_clss = map_dict(top_clss, cat_to_name, trn_img_dataset)
    top_probe.detach_()
    top_probe.squeeze_()
    return list(top_clss.values()), top_probe.numpy().tolist()
    # TODO: Implement the code to predict the class from an image file
print(deduct('flowers/train/11/image_03101.jpg',img_prcs_mdl.cpu()))
print(deduct(r"img_datasets\train\1\image_06738.jpg",img_prcs_mdl))
def imshow(image, ax=None, title=None):
    """Imshow for Tensor."""
    if ax is None:
        fig, ax = plt.subplots()
    
    # PyTorch tensors assume the color channel is the first dimension
    # but matplotlib assumes is the third dimension
    image = image.numpy().transpose((1, 2, 0))
    
    # Undo preprocessing
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    image = std * image + mean
    
    # Image needs to be clipped between 0 and 1 or it looks like noise when displayed
    image = np.clip(image, 0, 1)
    
    ax.imshow(image)
    
    return ax


#imshow(predict(r"E:\important_files_py\Python Scripts\Artificial_Intelligence\img_datasets\train\2\image_05098.jpg",img_prcs_mdl))


# %%

# %%
