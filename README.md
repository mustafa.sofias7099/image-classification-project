![image](https://www.swishschool.com/w/wp-content/uploads/2018/04/%D8%A8%D8%B3%D9%85-%D8%A7%D9%84%D9%84%D9%87-%D8%A7%D9%84%D8%B1%D8%AD%D9%85%D9%86-%D8%A7%D9%84%D8%B1%D8%AD%D9%8A%D9%85-3.jpg)
# Image Classification Project


## Getting started
    ╚ This file is implemented to be used for image classification; Mainly the model is separated for 3 main sectors;       
    Training, Validation and Testing phases; For training phase you might use training and validation imagesets in parallel,
    While training you should emphasize that changes that might affect the accuracy are abbreviately can be stated in 3 main points:
    • Data Augementation: Whereas data <images in this case> are exposed for multiple transformations like flipping or 
                          cropping images; These changes affect accumulatively in the inference accuracy.
    • Number of Nodes:    which precisely determines how are the images been segmented into multiple parts;
                         ▌Note that: these nodes determine each segment of the image which is converted to the format of
                                     Tensor.
    • Number of hidden layers: Determine number of compositions of the specified images where they are  being composed 
                               These layers are considered the final determination of the output; with increasing their
                               count; processing performance might be slower than usual.

(#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:67f02bc2caad8b425266c256312d41b7?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:67f02bc2caad8b425266c256312d41b7?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:67f02bc2caad8b425266c256312d41b7?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mustafa.sofias7099/image-classification-project.git
git branch -M main
git push -uf origin main
```




![image](https://i.ibb.co/F4zcTKY/New-Project.png)
***




## Name
Introduction to Neural Network architecture structuring.

## Description
The module is abbreviately discussing and focusing on How to build Neural Networks starting from using module `torch.nn `
which composed with the function **_Linear_** which takes mainly 2 arguments; **A → B** input weights and the output of the same layer; The output of this layer; is the input of the next layer.








## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.



## Authors and acknowledgment
> **Alhamdulillah** Rabbim Firstly and Eventually; and For _Eng.Heba_ Udacity course collegue; and Udacity support team. 

## License
For open source projects, say how it is licensed.



